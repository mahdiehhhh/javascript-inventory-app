
import CategoryView from "./CategoryView.js";

import productView from './ProductView.js'

document.addEventListener('DOMContentLoaded',()=>{
    CategoryView.setApp();

    productView.setApp();

    CategoryView.createCategoriesList();
    
    productView.createProductsList(productView.products);
})
class App{}