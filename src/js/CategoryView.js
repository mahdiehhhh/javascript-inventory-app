import Storage from "./Storage.js";

const categoryTitle = document.querySelector('#category-title');

const categoryDescription = document.querySelector('#category-description');

const newCategory = document.querySelector('#newCategory');

const categoryToggleBtn = document.getElementById('toggle-add-category');

const categoryWrapper = document.getElementById('category-wrapper');

class CategoryView {
    constructor() {
        newCategory.addEventListener('click', (e) => this.addNewCategory(e));

        categoryToggleBtn.addEventListener('click',(e) => this.toggleAddCategory(e));

        this.categories = [];
    }

    addNewCategory(event) {
        event.preventDefault();
        const title = categoryTitle.value;
        const description = categoryDescription.value;
        if (!title || !description) return;
        Storage.saveCategory({
            title,
            description
        })
        this.categories = Storage.getAllCategories();
        this.createCategoriesList();
        categoryTitle.value ='',
        categoryDescription.value = '';
    }

    setApp() {
        this.categories = Storage.getAllCategories();
    }

    createCategoriesList() {
        let result = `<option value="">select Category</option>`;
        this.categories.forEach((element) => {
            result += `<option class="bg-slate-500 text-slate-300" value="${element.id}">${element.title}</option>`;
        });
        const productCategory = document.querySelector('#product-category');
        productCategory.innerHTML = result;
    }

    toggleAddCategory(e){
        e.preventDefault();
        categoryWrapper.classList.toggle('hidden');
    }
}

export default new CategoryView();