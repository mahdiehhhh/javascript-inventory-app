const categor = [{
        id: 1,
        title: 'frontEnd1',
        description: 'haaaaaaaahahahaha',
        createdAt: '2022-10-01T10:47:26.889Z'
    },
    {
        id: 2,
        title: 'frontEnd2',
        description: 'haaaaaaaahahahaha',
        creatcreatedAtedAt: '2022-9-01T10:47:26.889Z'
    },
    {
        id: 3,
        title: 'frontEnd3',
        description: 'haaaaaaaahahahaha',
        createdAt: '2022-11-01T10:47:26.889Z'
    },
    {
        id: 4,
        title: 'frontEnd3',
        description: 'haaaaaaaahahahaha',
        createdAt: '2022-8-01T10:47:26.889Z'
    },
];

const products = [{
    id: 1,
    title: 'hi',
    quantity: 1,
    Category: 'hi',
    createdAt: '2022-9-01T10:47:26.889Z'
}];

export default class Storage {
    static getAllCategories() {
        const savedCategories = JSON.parse(localStorage.getItem("category")) || [];
        const sortedCategories = savedCategories.sort((a, b) => {
            return new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1;
        });
        return sortedCategories;
    }

    static saveCategory(categoryToSave) {
        const savedCategories = Storage.getAllCategories()

        const ExistedCategory = savedCategories.find((c) => c.id === categoryToSave.id);

        if (ExistedCategory) {
            ExistedCategory.title = categoryToSave.title;
            ExistedCategory.description = categoryToSave.description;
        } else {
            categoryToSave.id = new Date().getTime();
            categoryToSave.createdAt = new Date().toISOString();
            savedCategories.push(categoryToSave)
        }

        localStorage.setItem('category', JSON.stringify(savedCategories));
    }

    static getAllProducts(sort = "newest") {
        const saveProducts = JSON.parse(localStorage.getItem('products')) || [];
        const sortProducts = saveProducts.sort((a, b) => {
            if (sort === 'newst') {
                return new Date(a.createdAt) > new Date(b.createdAt) ? -1 : 1
            } else if (sort === 'oldest') {
                return new Date(a.createdAt) > new Date(b.createdAt) ? 1 : -1
            }
        });
        return sortProducts;
    }

    static saveProducts(productTosave) {
        const saveProducts = Storage.getAllProducts();

        const ExistedProduct = saveProducts.find((p) => p.id === productTosave.id);

        if (ExistedProduct) {
            ExistedProduct.title = productTosave.title;
            ExistedProduct.quantity = productTosave.quantity;
            ExistedProduct.Category = productTosave.Category;
        } else {
            productTosave.id = new Date().getTime();
            productTosave.createdAt = new Date().toISOString();
            saveProducts.push(productTosave)
        }
        localStorage.setItem('products', JSON.stringify(saveProducts))
    }

    static deleteProduct(id) {
        const allproducts = Storage.getAllProducts();
        const filterProduct = allproducts.filter((p) => p.id !== parseInt(id));
        localStorage.setItem("products", JSON.stringify(filterProduct));
    }


}